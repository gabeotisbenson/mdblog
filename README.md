# MDBlog
MDBlog is about as simple as it gets.  It's a simple Express app that renders markdown files as HTML.

## Installation
Clone the repo, enter the directory, run `npm i` to install dependencies, and then start the application using `node index.js`

## Theming
To style your blog, simply add your desired styles to `style.scss`, which will be parsed and loaded by the user.
