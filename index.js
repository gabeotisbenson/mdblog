// Required modules
const express	= require('express');
const fs			= require('fs');
const marked	= require('marked');
const path		= require('path');
const sass		= require('node-sass');

// Turn on app
const app = express();

// Load our template and style files\
const errorMd = fs.readFileSync(path.join(__dirname, 'error.md'), 'utf8');
const style = sass.renderSync({
	data: fs.readFileSync(path.join(__dirname, 'style.scss'), 'utf8') || '//'
}).css.toString();
const template = fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8');

// Function to clean up the filename
const sanitizeFilename = (str) => {
	// Strip out dot-segments
	if (str.indexOf('../') !== -1) str = str.replace(/\.\.\//g, '');
	// Add .md if doesn't exist
	if (!/\.md$/.test(str)) str += '.md';

	return str;
};

// The root request
app.get('/', (req, res) => {
	// Set the content-type to html
	res.set('Content-Type', 'text/html');
	// Read the posts directory
	fs.readdir(path.join(__dirname, 'posts'), (err, files) => {
		// Print any error to console
		if (err) console.error(err);
		// If files were found
		if (files) {
			// Create our output
			let output = '# Posts\n';
			// Then loop through each of the files
			files.forEach((file, index) => {
				// Adding a link to the output
				output += '- [' + file.replace(/\.md$/, '') + '](' + file + ')\n';
				// And if it's the last iteration of the loop, render our content
				if (index === files.length - 1) res.send(marked(output));
			});
		}
	});
});

// The stylesheet request handler
app.get('/style.css', (req, res) => {
	// Set the content type to css
	res.set('Content-Type', 'text/css');
	// And send the style content!
	res.send(style);
});

// File request handler
app.get('/:file', (req, res) => {
	// Set the content type to html
	res.set('Content-Type', 'text/html');
	// Sanitize the filename
	const filename = sanitizeFilename(req.params.file);
	// Check if the file exists
	if (fs.existsSync(path.join(__dirname, 'posts', filename))) {
		// File exists, so render it and send it
		res.send(template.replace('#CONTENT#', marked(fs.readFileSync(path.join(__dirname, 'posts', filename), 'utf8'))));
	} else {
		// File does not exist, so render an error
		res.send(template.replace('#CONTENT#', marked(errorMd)));
	}
});

// Start listening!
app.listen(3000, () => {
	console.log('Application listening on port 3000');
});
